<?php
/**
 * Created by PhpStorm.
 * User: ademaydin
 * Date: 22.01.2019
 * Time: 10:18
 */

namespace Project\CommonBundle\Helper;


class ServiceHelper
{
    CONST SABRE_GDS_PROVIDERS = [
        'O6', 'BI', 'ZI', 'BT', 'UX', 'VN', 'AY', 'EL', '1X', '2D', '2L', '2M', '2N', '3E', '3M', '3S', '4M', '4V', '5F', '5U', '5Z',
        '7J', '7M', '7N', '7W', '8L', '9F', '9N', 'B0', 'B4', 'BB', 'BD', 'BJ', 'BM', 'CC', 'CG', 'CU', 'D2',
        'E7', 'EN', 'F7', 'FA', 'FJ', 'FM', 'GS', 'H9', 'J9', 'JD', 'JF', 'JL', 'JY', 'KA', 'L6', 'LM', 'LP',
        'MI', 'MR', 'MW', 'NU', 'OB', 'PD', 'PE', 'PK', 'PZ', 'QH', 'R7', 'RA', 'RX', 'S4', 'SB', 'T3', 'TA',
        'UB', 'UK', 'UO', 'UU', 'V4', 'VJ', 'WX', 'WZ', 'XG', 'XL', 'Y7', 'Z9', 'PG', 'M9', 'MN', '9V', 'MU',
        'B5', 'P9', '0B', 'TX', '2I', '2K', '2J', '3G', '3U', '4Q', '4U', '5H', '5N', '5Q', '5W', '6H',
        '7F', '7I', '7R', '7V', '8M', '8Q', '8U', '9K', '9U', '9W', 'A3', 'A9', 'AA', 'AC', 'AD', 'AE', 'AF',
        'AH', 'AM', 'AR', 'AS', 'AT', 'AV', 'AZ', 'B2', 'B7', 'BA', 'BE', 'BG', 'BH', 'BL', 'BP', 'BR', 'BV',
        'BW', 'CI', 'CM', 'CX', 'CY', 'D6', 'DE', 'DL', 'DN', 'EI', 'EK', 'EQ', 'ET', 'EW', 'EY', 'FB', 'FZ',
        'G3', 'GA', 'GF', 'GK', 'GQ', 'GR', 'H2', 'HA', 'HM', 'HO', 'HR', 'HU', 'HY', 'HZ', 'IB', 'IE', 'IY',
        'IZ', 'J2', 'J8', 'JJ', 'JP', 'JU', 'K2', 'K5', 'K6', 'KC', 'KE', 'KK', 'KL', 'KM', 'KU', 'KP', 'KQ',
        'KX', 'LG', 'LI', 'LO', 'LR', 'LX', 'LH', 'LY', 'ME', 'MH', 'MK', 'MS', 'NF', 'NH', 'NP', 'NT',
        'NX', 'OK', 'OM', 'OR', 'OS', 'OU', 'OV', 'OZ', 'P0', 'PR', 'PS', 'PU', 'PX', 'PY', 'PW', 'QR', 'QU',
        'QV', 'R3', 'RJ', 'RO', 'RQ', 'S2', 'S3', 'SA', 'SC', 'SN', 'SP', 'SU', 'SQ', 'SS', 'SX', 'SV', 'SY',
        'TB', 'TF', 'TG', 'TJ', 'TK', 'TN', 'TM', 'TP', 'TS', 'TU', 'TZ', 'UA', 'UL', 'UM', 'UP', 'UT', 'W2',
        'V0', 'V2', 'V3', 'V7', 'VR', 'VS', 'VW', 'VX', 'VY', 'W3', 'W5', 'WM', 'WS', 'XK', /*'XQ',*/ 'YE',
        'YM', 'YV', 'Z6', 'Z8', 'ZH', 'DV', 'MT', 'S7', 'AI', 'WY', 'XY', 'CZ'
    ];

    CONST PERMITTED_PROVIDERS = [
        //'XY', 'ST', 'WB'
        'TK', 'KC','SU','HY','KK','XY'
    ];

    CONST SUPPLIER = [
        'DY',
        'BF',
        'DX',
        'EG',
        'PF',
        'NK',
        'Y4',
        'U2',
        'FR',
        'PD',
        'BY',
        'EW',
        'EI',
        '0B',
        'BV',
        'BE',
        'IG',
        'GQ',
        'HV',
        'V7',
        'VY',
        'DI'
    ];

    /*CONST SUPPLIER = [
        'A3' => 'aegeanairlines',
        'A7' => 'aereocalafia',
        'EI' => 'aerlingus',
        'ZZ' => 'aerocrs',
        'G9' => 'airarabia',
        'E5' => 'airarabiaegypt',
        '9P' => 'airarabiajordan',
        '3O' => 'airarabiamaroc',
        'AK' => 'airasiaarms',
        'AB' => 'airberlin',
        'PA' => 'airblue',
        'AC' => 'aircanada',
        '3C' => 'airchathams',
        //'NY' => 'airicelandconnect',
        'IX' => 'airindiaexpress',
        'NZ' => 'airnewzealandweb',
        'TL' => 'airnorth',
        'IS' => 'aisairlines',
        'AS' => 'alaskaairlines',
        'AA' => 'americanairlines',
        'NH' => 'ana',
        'HB' => 'asiaatlanticairlines',
        '5O' => 'aslairlinesfrance',
        'L5' => 'atlasatlantiqueairlines',
        'UI' => 'auricair',
        'GR' => 'aurigny',
        'OS' => 'austrianairlines',
        'O6' => 'aviancabrazil',
        'AD' => 'azul',
        '0B' => 'blueair',
        'BV' => 'bluexpress',
        'BA' => 'britishairways',
        'SN' => 'brusselsairlines',
        'MO' => 'calmair',
        //'KR' => 'cambodiaairways',
        'BD' => 'cambodiabayonairlines',
        '5J' => 'cebupacific',
        '6Q' => 'chamwings',
        'QG' => 'citilink',
        'WX' => 'cityjet',
        'V9' => 'citywingb2b',
        'CO' => 'cobaltair',
        'DE' => 'condor',
        'CD' => 'corendon',
        'XC' => 'corendonairlines',
        'D3' => 'daalloairlines',
        'DX' => 'dat',
        'ZE' => 'eastarjet',
        'T3' => 'easternairways',
        'WK' => 'edelweissair',
        'E4' => 'enterair',
        'U2' => 'ezy',
        'FN' => 'fastjet',
        'AY' => 'finnair',
        'FY' => 'firefly',
        'BE' => 'flybe',
        'FZ' => 'flydubai',
        'EG' => 'flyernest',
        'VP' => 'flyme',
        'XY' => 'flynas',
        'FA' => 'flysafair',
        'MT' => 'flythomascook',
        'BF' => 'frenchblue',
        'F9' => 'frontierairlines',
        'JH' => 'fujidreamairlines',
        'ST' => 'germania',
        'EW' => 'germanwings',
        'G8' => 'goair',
        'G3' => 'gol',
        'Y5' => 'goldenmyanmarairlines',
        'UO' => 'hkexpress',
        '6E' => 'indigo',
        '4O' => 'interjet',
        '03' => 'involatus',
        'J9' => 'jazeeraairways',
        'QD' => 'jcairlines',
        '7C' => 'jejuair',
        'LS' => 'jet2',
        'TB' => 'jetairfly',
        'JQ' => 'jetstar',
        'XE' => 'jetsuitex',
        'LJ' => 'jinairweb',
        '3J' => 'jubbaairways',
        'RQ' => 'kamair',
        'MN' => 'kululaprepay',
        'LQ' => 'lanmeiairlines',
        'JT' => 'lionair',
        'SL' => 'lionairthai',
        'LM' => 'loganair',
        'LH' => 'lufthansa',
        'W5' => 'mahanair',
        'OD' => 'malindoair',
        'JE' => 'mango',
        '7Y' => 'mannyadanarponairlines',
        '7M' => 'mayair',
        'IG' => 'meridiana',
        'NO' => 'neosair',
        '2N' => 'nextjetweb',
        'AQ' => 'nineair',
        'DD' => 'nokair',
        'XW' => 'nokscoot',
        'DY' => 'norwegian',
        'BJ' => 'nouvelair',
        'OF' => 'officinedelturismo',
        'OA' => 'olympicair',
        '8Q' => 'onurair',
        'OX' => 'orientthaiairlines',
        //'8Y' => 'panpacificairlines',
        'MM' => 'peachairlines',
        'PC' => 'pegasus',
        'PK' => 'pia',
        'DP' => 'pobeda',
        'PD' => 'porter',
        'PF' => 'primeraair',
        'ZL' => 'regionalexpress',
        'FR' => 'ryanair',
        'OV' => 'salamair',
        'TR' => 'scoot',
        'NL' => 'shaheenair',
        'SH' => 'sharpairlines',
        'HK' => 'skippers',
        'H2' => 'skyairline',
        'GQ' => 'skyexpress',
        'BC' => 'skymark',
        'SX' => 'skywork',
        'QS' => 'smartwings',
        'S8' => 'soundsair',
        'SG' => 'spicejet',
        'NK' => 'spirit',
        '9C' => 'springairlines',
        'IJ' => 'springairlinesjapan',
        'SJ' => 'sriwijayaairgroup',
        '7G' => 'starflyer',
        '2I' => 'starperu',
        'XQ' => 'sunexpress',
        'S6' => 'sunriseairways',
        'LX' => 'swiss',
        'BY' => 'thomsonfly',
        'TT' => 'tigerairaustralia',
        'IT' => 'tigerairtaiwan',
        'HV' => 'transavia',
        //'OR' => 'tuiairlinesnetherlands',
        'X3' => 'tuifly',
        'TW' => 'twayair',
        'JW' => 'vanilla',
        'VJ' => 'vietjet',
        'VX' => 'virginamerica',
        'VA' => 'virginaustralia',
        'VB' => 'vivaaerobus',
        'VV' => 'vivaairperu',
        'FC' => 'vivacolombia',
        'VO' => 'vlmairlines',
        'Y4' => 'volaris',
        'V7' => 'volotea',
        'VY' => 'vueling',
        'WS' => 'westjet',
        'P5' => 'wingo',
        'W6' => 'wizzair',
        'WW' => 'wowair',
        'YT' => 'yetiair',
    ];*/
}