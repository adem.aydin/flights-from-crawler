<?php
/**
 * Created by PhpStorm.
 * User: ulugbek
 * Date: 3/13/15
 * Time: 16:58
 */

namespace Project\CommonBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Project\CommonBundle\Helper\ServiceHelper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;


class FlightsFromDataParserCommand extends ContainerAwareCommand
{
    PUBLIC CONST URL = 'https://www.flightsfrom.com/{AIRPORT_CODE}/destinations?dateMethod=day&dateFrom={DATE}&dateTo={DATE}';

    PUBLIC CONST SCHEDULE_LIST_URL = 'https://www.flightsfrom.com/api/schedulelist/%s/%s';

    PUBLIC CONST REQUEST_COLLECTION_NAME = 'routes_requests';

    /** @var EntityManager $em */
    protected $em;

    /** @var Connection $conn*/
    protected $conn;

    /** @var ConsoleOutput $consoleOutput*/
    protected $consoleOutput;

    /** @var \Memcached $memcached*/
    protected $memcached;

    /** @var \MongoClient $mongo*/
    protected $mongo;

    PUBLIC CONST AIRPORT_CODES = [
        ['IST'], ['ADB']
    ];

    CONST REQUEST_LIMIT = 50;

    CONST GDS = [
        'sabre' => ServiceHelper::SABRE_GDS_PROVIDERS,
        //'galileo' => ServiceHelper::PERMITTED_PROVIDERS,
        'travelfusion' => ServiceHelper::SUPPLIER
    ];

    CONST AIRLINES = [
        'PC' => 'pegasus',
        'KK' => 'atlas',
        'TK' => 'thy',
        'XQ' => 'sunexpress',
        '8Q' => 'onurair',
        //'LH' => 'lufthansa',
        'XC' => 'corendon',
        'ST' => 'germania'
    ];

    CONST MAX_DATE = '2019-12-31';

    protected function configure()
    {
        $this
            ->setName('flights:from:parser:command')
            ->addOption('startIndex', null, InputOption::VALUE_OPTIONAL, 'Start Index')
            ->setDescription('Virtual interlining default route builder command');
    }

    protected function getCrawlerRoutes($startIndex)
    {
        $airportCodes = $this->conn->fetchAll("SELECT * FROM crawler_routes ORDER BY id ASC LIMIT {$startIndex},9354", [ //4677
            $this->countryCodes], [Connection::PARAM_STR_ARRAY]);

        return $airportCodes;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     *
     * @throws \MongoException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set("memory_limit", "-1");

        $this->setVariables();

        $startTime = microtime(true);

        $output->writeln('<info>Started At: ' . date('Y-m-d H:i:s') . '</info>');

        $this->setVariables();

        $startIndex = $input->getOption('startIndex');

        $routes = $this->getCrawlerRoutes($startIndex);

        $requestCount = 0;

        $size = count($routes) * count($this->getRequestDates());

        foreach ($this->getRequestDates() as $date) {

            foreach ($routes as $index => $route) {

                var_dump("Remaining: " . ($size - $requestCount));

                $hash = $route['from_airport'] . '_' . $route['to_airport'] . '_' . $date;

                try {
                    $requestData = $this->mongo->crawler->{self::REQUEST_COLLECTION_NAME}->findOne([
                        'hash' => $hash
                    ]);

                    $scheduleList = \GuzzleHttp\json_decode($requestData['response'], true);

                    $this->save($scheduleList);

                    $this->consoleOutput->writeln($hash . ' memcache saved');
                } catch (\Exception $exception) {
                    $this->consoleOutput->writeln($exception->getMessage());
                }

                $requestCount++;
            }

        }

        $endTime = ceil((microtime(true) - $startTime));

        $output->writeln([
            '<info>Finish At: ' . date('Y-m-d H:i:s') . '</info>',
            '<info>It took ' . $endTime . ' second.</info>'
        ]);
    }

    /**
     * @param $scheduleList
     */
    public function save($scheduleList)
    {
        $response = $scheduleList['response'];

        $from = current($response['result'])['iata_from'];

        $to = current($response['result'])['iata_to'];

        $data = [
            'from' => $from,
            'to' => $to,
            'status' => 0,
            'check' => 0,
            'date' => $response['date'],
            'month' => (new \DateTime($response['date']))->format('m'),
            'created_at' => new \MongoDate(),
            'updated_at' => new \MongoDate()
        ];

        $allData = [];

        if (!empty($response['result'])) {
            $airlines = [];
            foreach ($response['result'] as $flight) {
                $providers = $this->findProviderByAirlineCode($flight['carrier'], $from, $to);

                $data = array_merge($data, $providers);

                $airlines[] = $flight['carrier'];
            }

            $data['airlines'] = implode(',', array_unique($airlines));

            $allData[] = $data;
        }

        foreach ($response['sheduledays'] as $sheduleday) {
            $data = [
                'from' => $from,
                'to' => $to,
                'status' => 0,
                'check' => 0,
                'date' => (new \DateTime($sheduleday['selected_date']))->format('Y-m-d'),
                'month' => (new \DateTime($data['date']))->format('m'),
                'created_at' => new \MongoDate(),
                'updated_at' => new \MongoDate()
            ];

            if (empty($sheduleday['result'])) {
                continue;
            }

            $airlines = [];

            foreach ($sheduleday['result'] as $flight) {
                $airlines[] = $flight['carrier'];

                $providers = $this->findProviderByAirlineCode($flight['carrier'], $from, $to);

                if (!$data['from'] || !$data['to']) {
                    $data['from'] = current($sheduleday['result'])['iata_from'];

                    $data['to'] = current($sheduleday['result'])['iata_to'];
                }

                $data = array_merge($data, $providers);
            }

            $data['airlines'] = implode(',', array_unique($airlines));

            $allData[] = $data;
        }

        if (!empty($allData)) {
            $this->saveMongo($allData);
        }
    }

    public function findProviderByAirlineCode(string $airlineCode, $from, $to)
    {
        $providers = [];

        foreach (self::GDS as $gds => $GDS_AIRLINES) {
            if ($airlineCode === 'TK' && $this->isDomestic($from, $to)) {
                continue;
            }

            if (in_array($airlineCode, $GDS_AIRLINES)) {
                $providers[$gds] = 0;
            }
        }

        if (isset(self::AIRLINES[$airlineCode])) {
            $provider = self::AIRLINES[$airlineCode];

            if (($provider === 'thy' && !$this->isDomestic($from, $to)) || ($provider === 'atlas' && $this->isDomestic($from, $to))) {

            } else {
                $providers[$provider] = 0;
            }
        }

        return $providers;
    }

    public function isDomestic($from, $to)
    {
        if (!in_array($from, $this->trAirports) || !in_array($to, $this->trAirports)) {
            return false;
        }

        return true;
    }

    public function getRequestDates()
    {
        $dates = [];

        $mouth = 0;

        while (true) {
            $date = (new \DateTime())->modify("+{$mouth} month");

            $lastDate = $date->format('Y-m-t');

            if ($date > new \DateTime(self::MAX_DATE)) {
                break;
            }

            $dates[] = (new \DateTime($lastDate))
                ->modify('-1 week')->format('Y-m-d');

            $mouth++;
        }

        return $dates;
    }

    public function saveMongo($message = array(), $collection = null)
    {
        $date = new \DateTime();

        $collection = ($collection) ? $collection : 'routes';

        /** @var \MongoClient $mongo */
        $mongo = $this->getContainer()->get('mongo_client');

        $mongo->selectCollection('crawler', $collection)->batchInsert($message);
    }

    /**
     * @return void
     */
    protected function setVariables(): void
    {
        $this->em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $this->conn = $this->em->getConnection();
        $this->consoleOutput = new ConsoleOutput();
        $this->memcached = $this->getContainer()->get('memcached_client');
        $this->mongo = $this->getContainer()->get('mongo_client');
    }

    private $countryCodes = [
        'AL',
        'AD',
        'AM',
        'AT',
        'AZ',
        'BY',
        'BE',
        'BA',
        'BG',
        'HR',
        'CY',
        'CZ',
        'DK',
        'EE',
        'FI',
        'FR',
        'GE',
        'DE',
        'GR',
        'HU',
        'IS',
        'IE',
        'IT',
        'LV',
        'LI',
        'LT',
        'LU',
        'MK',
        'MT',
        'MD',
        'MC',
        'ME',
        'NL',
        'NO',
        'PL',
        'RO',
        'RU',
        'SM',
        'RS',
        'SK',
        'SI',
        'ES',
        'SE',
        'CH',
        'TR',
        'UA',
        'GB',
        'VA',
        'XK',
        'PT',
        'IL',
        'SA',
        'EG',
        'AE',
        'QA',
        'KW',
        'JO',
        'US',
        'CA',
        'BR',
        'CL',
        'CO',
        'CR',
        'UZ',
        'KZ',
        'TM',
        'KG',
        'PK',
        'TJ',
        'IN',
        'TH',
        'CN',
        'JP',
        'KR',
        'SG',
        'ID',
        'MY',
        'MV',
        'MN',
        'PH',
        'MG',
        'SC',
        'VI',
        'VG',
        'MU',
        'ZA',
        'MZ',
        'TN',
        'MA',
        'DZ',
        'GH',
        'KE',
        'ZM',
        'CD',
        'CI',
        'NG',
        'SN',
        'SO',
        'ML',
    ];

    private $trAirports = [
        'ADF',
        'ADB',
        'AJI',
        'AOE',
        'AYT',
        'IST',
        'BDM',
        'BAL',
        'BGG',
        'GKD',
        'CKZ',
        'DNZ',
        'SZF',
        'ONQ',
        'TEQ',
        'DLM',
        'DIY',
        'EDO',
        'EZS',
        'MLX',
        'ASR',
        'ERC',
        'ERZ',
        'ESB',
        'ESK',
        'VAN',
        'GZP',
        'GNY',
        'HTY',
        'XHQ',
        'IGD',
        'KCM',
        'NAV',
        'KSY',
        'KFS',
        'KCO',
        'KYA',
        'MQM',
        'MQJ',
        'MZH',
        'BJV',
        'MSR',
        'GZT',
        'SAW',
        'ADA',
        'SSX',
        'SFQ',
        'SXZ',
        'NOP',
        'NKT',
        'VAS',
        'ISE',
        'TJK',
        'TZX',
        'USQ',
        'YEI',
        'KZR',
        'OGU',
        'YKO'
    ];
}
