<?php
/**
 * Created by PhpStorm.
 * User: ulugbek
 * Date: 3/13/15
 * Time: 16:58
 */

namespace Project\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('test:command')
            ->setDescription('Virtual interlining default route builder command');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \MongoClient $mongo*/
        $mongo = $this->getContainer()->get('mongo_client');

        $memcached = $this->getContainer()->get('memcached_client');


        $memcached->set('adem', true);

        $a  = $memcached->get('adem');

        var_dump($a);

        die;
    }
}
