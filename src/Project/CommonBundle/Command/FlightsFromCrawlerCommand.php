<?php
/**
 * Created by PhpStorm.
 * User: ulugbek
 * Date: 3/13/15
 * Time: 16:58
 */

namespace Project\CommonBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Project\CommonBundle\Helper\ServiceHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;


class FlightsFromCrawlerCommand extends ContainerAwareCommand
{
    PUBLIC CONST URL = 'https://www.flightsfrom.com/{AIRPORT_CODE}/destinations?dateMethod=day&dateFrom={DATE}&dateTo={DATE}';

    PUBLIC CONST SCHEDULE_LIST_URL = 'https://www.flightsfrom.com/api/schedulelist/%s/%s';

    PUBLIC CONST REQUEST_COLLECTION_NAME = 'routes_requests';

    PUBLIC CONST COLLECTION_NAME = 'routes_new';

    /** @var EntityManager $em */
    protected $em;

    /** @var Connection $conn*/
    protected $conn;

    /** @var ConsoleOutput $consoleOutput*/
    protected $consoleOutput;

    /** @var \Memcached $memcached*/
    protected $memcached;

    PUBLIC CONST AIRPORT_CODES = [
        ['IST'], ['ADB']
    ];

    CONST REQUEST_LIMIT = 25;

    CONST GDS = [
        'sabre' => ServiceHelper::SABRE_GDS_PROVIDERS,
        //'galileo' => ServiceHelper::PERMITTED_PROVIDERS,
        'travelfusion' => ServiceHelper::SUPPLIER
    ];

    CONST AIRLINES = [
        'PC' => 'pegasus',
        'KK' => 'atlas',
        'TK' => 'thy',
        'XQ' => 'sunexpress',
        '8Q' => 'onurair',
        //'LH' => 'lufthansa',
        'XC' => 'corendon',
        'ST' => 'germania'
    ];

    CONST MAX_DATE = '2019-12-31';

    protected function configure()
    {
        $this
            ->setName('flights:from:crawler:command')
            ->addOption('startIndex', null, InputOption::VALUE_OPTIONAL, 'Start Index')
            ->setDescription('Virtual interlining default route builder command');
    }

    protected function getAirportCodes($startIndex)
    {
        $airportCodes = $this->conn->fetchAll("SELECT AirportCode FROM flight_airport_codes WHERE CountryCode IN(?) LIMIT {$startIndex},633", [
            $this->countryCodes], [Connection::PARAM_STR_ARRAY]);

        return array_merge(array_column($airportCodes, 'AirportCode'), ['BGW', 'BSR', 'IKA', 'TBZ', 'EBL', 'BEY', 'SYD', 'MEL']);
    }

    protected function getCrawlerRoutes($startIndex)
    {
        $airportCodes = $this->conn->fetchAll("SELECT * FROM crawler_routes ORDER BY id ASC LIMIT {$startIndex},9354", [ //4677
            $this->countryCodes], [Connection::PARAM_STR_ARRAY]);

        return $airportCodes;
    }

    private $keys = null;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set("memory_limit", "-1");
        $this->setVariables();

        $startTime = microtime(true);
        $output->writeln('<info>Started At: ' . date('Y-m-d H:i:s') . '</info>');

        $this->setVariables();

        $startIndex = $input->getOption('startIndex');

        $routes = $this->getCrawlerRoutes($startIndex);

        $requestCount = 0;

        $data = [];

        $size = count($routes) * count($this->getRequestDates());

        foreach ($this->getRequestDates() as $date) {

            foreach ($routes as $index => $route) {

                var_dump("Remaining: " . ($size - $requestCount));

                $from = $route['from_airport'];

                $to = $route['to_airport'];

                if ($this->hasCacheRoute($route['from_airport'], $route['to_airport'], $date)) {
                    //$this->memcached->delete(self::generateMemcacheKey($route['from_airport'], $route['to_airport'], $date));
                    $this->writeLine($route['from_airport'] . '_' . $route['to_airport'] . '_' . $date . ' this airport has cache...');
                    continue;
                }

                if ($requestCount % self::REQUEST_LIMIT === 0 && $requestCount !==0) {
                    $this->writeLine('Sending Requests Index : ' . $requestCount);

                    $this->doSearch($data, true);

                    $data = [];
                }

                $data[] = [
                    'from' => $from,
                    'to' => $to,
                    'date' => $date
                ];

                $requestCount++;
            }

        }

        if (!empty($data)) {
            $this->writeLine('Sending Requests Index : ' . $requestCount);

            $this->doSearch($data, true);
        }

        $endTime = ceil((microtime(true) - $startTime));

        $output->writeln([
            '<info>Finish At: ' . date('Y-m-d H:i:s') . '</info>',
            '<info>It took ' . $endTime . ' second.</info>'
        ]);
    }

    private $detailData = [];

    protected function doSearch($data, $subRequest = false)
    {
        //$this->consoleOutput->writeln('<info>Async Search Started At: ' . date('Y-m-d H:i:s') . '</info>');

        $client = new Client();

        $promises = [];

        $requests = [];

        foreach ($data as $dataIndex => $_data) {

            if ($subRequest) {

                $key = $_data['from'] . '_' . $_data['to'] . '_' . $_data['date'];

                if ($this->memcached->get($key)) {

                    $this->consoleOutput->writeln($key . ' this key memcached exist for continue :)');

                    continue;
                }

                $url = sprintf(self::SCHEDULE_LIST_URL, $_data['from'], $_data['to']);

                $rand_key = array_rand($this->userAgents, 1);

                $userAgent = $this->userAgents[$rand_key];

                $request = new Request('POST', $url, [
                    'Content-Type' => 'application/json',
                    'User-Agent' => $userAgent
                ], \GuzzleHttp\json_encode([
                    'filters' => [
                        'countries' => [],
                        'airlines' => [],
                        'aircrafts' => [],
                        'timeofday' => [],
                        'duration' => [],
                        'dates' => [
                            'from' => $_data['date'],
                            'to' => $_data['date'],
                            'method' => 'day'
                        ]
                    ]
                ]));

                $requests[] = $request;

                $promises[] = $client->sendAsync($request);

                $this->writeLine($key . ' : started');
            } else {
                $airportCode = $_data['code'];

                $date = $_data['date'];

                $request = new Request('GET', self::generateUrl($airportCode, $date));

                $requests[] = $request;

                $promises[] = $client->sendAsync($request);

                $this->writeLine($date . ' : ' . $airportCode . ' : started');
            }
        }

        $responses = Promise\inspect_all($promises);

        $retryData = [];

        //$this->consoleOutput->writeln('<info>Async Search End At: ' . date('Y-m-d H:i:s') . '</info>');

        foreach ($responses as $index => $result) {

            $status = $result['state'];

            $response = ($status == Promise\PromiseInterface::REJECTED) ? $result['reason']->getResponse() : $result['value'];

            try {
                if (!$response instanceof Response) {
                    throw new \Exception('Response not instanceof Response !!');
                }

                if ($status == Promise\PromiseInterface::REJECTED && !$response instanceof Response) {
                    throw new \Exception('Failed to Request Rejected Index: '.$index);
                }

                if ($response instanceof Response && $response->getStatusCode() === 404) {
                    continue;
                }

                $result = $response->getBody()->getContents();

                preg_match('/\{.*\:\{.*\:.*\}\}/', $result, $matches);

                if (empty($matches)) {
                    throw new \Exception('Result empty');
                }

                $result = current($matches);

                $this->logRequest([
                    'hash' => $data[$index]['from'] . '_' . $data[$index]['to'] . '_' . $data[$index]['date'],
                    'from' => $data[$index]['from'],
                    'to' => $data[$index]['to'],
                    'date' => $data[$index]['date'],
                    'uri' => $requests[$index]->getUri()->__toString(),
                    'response' => $result,
                    'statusCode' => $response->getStatusCode(),
                    'completed' => 1, // todo change last command
                    'created_at' => new \MongoDate(),
                    'updated_at' => new \MongoDate()
                ]);

                if (!$subRequest) {
                    $crawler = new Crawler($result);

                    $fromAirportCode = self::parseH1AirportCode($crawler->filter('.airport-font-uppercase')->text());

                    $this->detailData = [];

                    $date = $data[$index]['date'];

                    $crawler->filter('#airport-destination-items li')->each(function (Crawler $node, $i) use ($fromAirportCode, $date) {

                        if (count($node)) {
                            $toAirportCode = $node->attr('data-iata');

                            $this->detailData[] = [
                                'from' => $fromAirportCode,
                                'to' => $toAirportCode,
                                'date' => $date
                            ];
                        }

                    });

                    $this->doSearch($this->detailData, true);
                } else {
                    $scheduleList = \GuzzleHttp\json_decode($result, true);
                    $this->save($scheduleList);

                    $key = $data[$index]['from'] . '_' . $data[$index]['to'] . '_' . $data[$index]['date'];

                    $this->memcached->set($key, true);

                    $this->consoleOutput->writeln($key . ' memcache saved');
                }
            } catch(\MongoDuplicateKeyException $e) {
                var_dump("duplicate key error: " . $e->getMessage());
            } catch(\Exception $e) {
                var_dump(get_class($e));

                if (strpos($e->getMessage(), 'The current node list is empty') === false && strpos($e->getMessage(), 'duplicate key error collection') === false) {
                    $retryClose = false;

                    if (isset($data[$index]['retryCount'])) {

                        if ($data[$index]['retryCount'] > 3) {
                            $retryClose = true;
                        }

                        $data[$index]['retryCount'] += 1;
                    } else {
                        $data[$index]['retryCount'] = 1;
                    }

                    if (!$retryClose) {
                        $retryData[] = $data[$index];
                        $this->writeLine('Retry array adding');
                    } else {
                        $this->writeLine('Retry continue limit finish');
                    }
                }

                var_dump($response->getBody()->getContents());

                $this->writeLine('ERROR PARENT *** Can not get result for : '. json_encode($data[$index]) . ' e  => ' . $e->getMessage());
            }

            if ($retryData) {
                $this->consoleOutput->writeln('<error>Sending Retry Requests... Retyr Count '.count($retryData).'</error>');
                $this->doSearch($retryData, $subRequest);
            }
        }
    }

    /**
     * @param array $data
     */
    public function logRequest(array $data)
    {
        $this->saveMongo([$data], self::REQUEST_COLLECTION_NAME);
    }

    public function findProviderByAirlineCode(string $airlineCode, $from, $to)
    {
        $providers = [];

        foreach (self::GDS as $gds => $GDS_AIRLINES) {
            if ($airlineCode === 'TK' && $this->isDomestic($from, $to)) {
                continue;
            }

            if (in_array($airlineCode, $GDS_AIRLINES)) {
                $providers[$gds] = 0;
            }
        }

        if (isset(self::AIRLINES[$airlineCode])) {
            $provider = self::AIRLINES[$airlineCode];

            if (($provider === 'thy' && !$this->isDomestic($from, $to)) || ($provider === 'atlas' && $this->isDomestic($from, $to))) {

            } else {
                $providers[$provider] = 0;
            }
        }

        return $providers;
    }

    /**
     * @param $from
     * @param $to
     * @param $date
     * @param $scheduleList
     */
    public function save($scheduleList)
    {
        $response = $scheduleList['response'];

        $from = current($response['result'])['iata_from'];

        $to = current($response['result'])['iata_to'];

        $data = [
            'from' => $from,
            'to' => $to,
            'status' => 0,
            'check' => 0,
            'date' => $response['date'],
            'month' => (new \DateTime($response['date']))->format('m'),
            'created_at' => new \MongoDate(),
            'updated_at' => new \MongoDate()
        ];

        $allData = [];

        if (!empty($response['result'])) {
            $airlines = [];
            foreach ($response['result'] as $flight) {
                $providers = $this->findProviderByAirlineCode($flight['carrier'], $from, $to);

                $data = array_merge($data, $providers);

                $airlines[] = $flight['carrier'];
            }

            $data['airlines'] = implode(',', array_unique($airlines));

            $allData[] = $data;
        }

        /*foreach ($response['sheduledays'] as $sheduleday) {
            $data = [
                'from' => $from,
                'to' => $to,
                'status' => 0,
                'check' => 0,
                'created_at' => new \MongoDate(),
                'updated_at' => new \MongoDate()
            ];

            $data['date'] = (new \DateTime($sheduleday['selected_date']))->format('Y-m-d');
            $data['month'] = (new \DateTime($data['date']))->format('m');

            if (empty($sheduleday['result'])) {
                continue;
            }

            $airlines = [];

            foreach ($sheduleday['result'] as $flight) {
                $airlines[] = $flight['carrier'];

                $providers = $this->findProviderByAirlineCode($flight['carrier'], $from, $to);

                if (!$data['from'] || !$data['to']) {
                    $data['from'] = current($sheduleday['result'])['iata_from'];

                    $data['to'] = current($sheduleday['result'])['iata_to'];
                }

                $data = array_merge($data, $providers);
            }

            $data['airlines'] = implode(',', array_unique($airlines));

            $allData[] = $data;
        }*/

        if (!empty($allData)) {
            $this->saveMongo($allData);
        }
    }

    public static function generateMemcacheKey($from, $to, $date)
    {
        return $from . '_' . $to . '_' . $date;
    }

    public function hasCacheRoute($from, $to, $date)
    {
        $key = self::generateMemcacheKey($from, $to, $date);

        return $this->memcached->get($key);
    }

    public function hasCache($airportCode)
    {
        foreach ($this->keys as $key) {
            if (strpos($key, $airportCode) === 0) {
                return true;
            }
        }

        return false;
    }

    public function isDomestic($from, $to)
    {
        if (!in_array($from, $this->trAirports) || !in_array($to, $this->trAirports)) {
            return false;
        }

        return true;
    }

    protected static function parseH1AirportCode($h1)
    {
        preg_match('/\([A-Z]{3}\)/', $h1, $matches);

        return trim(current($matches), ')(');
    }

    protected static function parseImageSrcAirlineCode($src)
    {
        preg_match('/[A-Z0-9]{2}_/', $src, $matches);

        return rtrim(current($matches), '_');
    }

    protected function crawlUrl(string $url)
    {
        $client = new Client();

        $response = $client->request('GET', $url, [
            'headers' => [
                'Cache-Control' => 'max-age=0',
                'If-Modified-Since' => 'Tue, 25 Dec 2018 01:08:18 GMT',
                'If-None-Match' => '"15b222-57dce594df318-gzip"',
                'Upgrade-Insecure-Requests' => 1,
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
            ]
        ]);

        $response->getResponse()->getBody();
    }

    /**
     * @param string $text
     *
     * @return void
     */
    private function writeLine(string $text): void
    {
        $this->consoleOutput->writeln($text);
    }

    /**
     * @param string $airportCode
     * @param string $date
     *
     * @return string
     */
    private static function generateUrl(string $airportCode, string $date): string
    {
        return str_replace(['{AIRPORT_CODE}', '{DATE}'], [$airportCode, $date], self::URL);
    }

    public function getRequestDates()
    {
        $dates = [];

        $mouth = 0;

        while (true) {
            $date = (new \DateTime())->modify('-2 day')->modify("+{$mouth} month");

            $lastDate = $date->format('Y-m-t');

            if ($date > new \DateTime(self::MAX_DATE)) {
                break;
            }

            $dates[] = (new \DateTime($lastDate))
                /*->modify('-1 week')*/->format('Y-m-d');

            $mouth++;
        }

        return $dates;
    }

    public function saveMongo($message = array(), $collection = null)
    {
        $date = new \DateTime();

        $date = $date->format(\DateTime::W3C);

        $collection = ($collection) ? $collection : self::COLLECTION_NAME;

        /** @var \MongoClient $mongo */
        $mongo = $this->getContainer()->get('mongo_client');

        $mongo->selectCollection('crawler', $collection)->batchInsert($message);
    }

    /**
     * @return void
     */
    protected function setVariables(): void
    {
        $this->em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $this->conn = $this->em->getConnection();
        $this->consoleOutput = new ConsoleOutput();
        $this->memcached = $this->getContainer()->get('memcached_client');
    }

    private $countryCodes = [
        'AL',
        'AD',
        'AM',
        'AT',
        'AZ',
        'BY',
        'BE',
        'BA',
        'BG',
        'HR',
        'CY',
        'CZ',
        'DK',
        'EE',
        'FI',
        'FR',
        'GE',
        'DE',
        'GR',
        'HU',
        'IS',
        'IE',
        'IT',
        'LV',
        'LI',
        'LT',
        'LU',
        'MK',
        'MT',
        'MD',
        'MC',
        'ME',
        'NL',
        'NO',
        'PL',
        'RO',
        'RU',
        'SM',
        'RS',
        'SK',
        'SI',
        'ES',
        'SE',
        'CH',
        'TR',
        'UA',
        'GB',
        'VA',
        'XK',
        'PT',
        'IL',
        'SA',
        'EG',
        'AE',
        'QA',
        'KW',
        'JO',
        'US',
        'CA',
        'BR',
        'CL',
        'CO',
        'CR',
        'UZ',
        'KZ',
        'TM',
        'KG',
        'PK',
        'TJ',
        'IN',
        'TH',
        'CN',
        'JP',
        'KR',
        'SG',
        'ID',
        'MY',
        'MV',
        'MN',
        'PH',
        'MG',
        'SC',
        'VI',
        'VG',
        'MU',
        'ZA',
        'MZ',
        'TN',
        'MA',
        'DZ',
        'GH',
        'KE',
        'ZM',
        'CD',
        'CI',
        'NG',
        'SN',
        'SO',
        'ML',
    ];

    private $trAirports = [
        'ADF',
        'ADB',
        'AJI',
        'AOE',
        'AYT',
        'IST',
        'BDM',
        'BAL',
        'BGG',
        'GKD',
        'CKZ',
        'DNZ',
        'SZF',
        'ONQ',
        'TEQ',
        'DLM',
        'DIY',
        'EDO',
        'EZS',
        'MLX',
        'ASR',
        'ERC',
        'ERZ',
        'ESB',
        'ESK',
        'VAN',
        'GZP',
        'GNY',
        'HTY',
        'XHQ',
        'IGD',
        'KCM',
        'NAV',
        'KSY',
        'KFS',
        'KCO',
        'KYA',
        'MQM',
        'MQJ',
        'MZH',
        'BJV',
        'MSR',
        'GZT',
        'SAW',
        'ADA',
        'SSX',
        'SFQ',
        'SXZ',
        'NOP',
        'NKT',
        'VAS',
        'ISE',
        'TJK',
        'TZX',
        'USQ',
        'YEI',
        'KZR',
        'OGU',
        'YKO'
    ];

    private $userAgents = [
        "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.25 (KHTML, like Gecko) Chrome/12.0.706.0 Safari/534.25",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Ubuntu/10.10 Chromium/12.0.703.0 Chrome/12.0.703.0 Safari/534.24",
        "Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.24 (KHTML, like Gecko) Ubuntu/10.10 Chromium/12.0.702.0 Chrome/12.0.702.0 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/12.0.702.0 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/12.0.702.0 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.699.0 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.699.0 Safari/534.24",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.698.0 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.697.0 Safari/534.24",
        "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.43 Safari/534.24",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.3 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.3 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.3 Safari/534.24",
        "Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.14 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.12 Safari/534.24",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.12 Safari/534.24",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Ubuntu/10.04 Chromium/11.0.696.0 Chrome/11.0.696.0 Safari/534.24",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.0 Safari/534.24",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.694.0 Safari/534.24",
        "Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.23 (KHTML, like Gecko) Chrome/11.0.686.3 Safari/534.23",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.21 (KHTML, like Gecko) Chrome/11.0.682.0 Safari/534.21",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.21 (KHTML, like Gecko) Chrome/11.0.678.0 Safari/534.21",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_7_0; en-US) AppleWebKit/534.21 (KHTML, like Gecko) Chrome/11.0.678.0 Safari/534.21",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
        "Mozilla/5.0 (Windows NT) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.669.0 Safari/534.20",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.19 (KHTML, like Gecko) Chrome/11.0.661.0 Safari/534.19",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.18 (KHTML, like Gecko) Chrome/11.0.661.0 Safari/534.18",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/534.18 (KHTML, like Gecko) Chrome/11.0.660.0 Safari/534.18",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.17 (KHTML, like Gecko) Chrome/11.0.655.0 Safari/534.17",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.17 (KHTML, like Gecko) Chrome/11.0.655.0 Safari/534.17",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.17 (KHTML, like Gecko) Chrome/11.0.654.0 Safari/534.17",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.17 (KHTML, like Gecko) Chrome/11.0.652.0 Safari/534.17",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.17 (KHTML, like Gecko) Chrome/10.0.649.0 Safari/534.17",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; de-DE) AppleWebKit/534.17 (KHTML, like Gecko) Chrome/10.0.649.0 Safari/534.17",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.82 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux armv7l; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204 Safari/534.16",
        "Mozilla/5.0 (X11; U; FreeBSD x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204 Safari/534.16",
        "Mozilla/5.0 (X11; U; FreeBSD i386; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204 Safari/534.16",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.134 Safari/534.16",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.134 Safari/534.16",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.134 Safari/534.16",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.134 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.133 Chrome/10.0.648.133 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.133 Chrome/10.0.648.133 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.127 Chrome/10.0.648.127 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.127 Safari/534.16",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.127 Safari/534.16",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.127 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.11 Safari/534.16",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru-RU) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.11 Safari/534.16",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.11 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.0 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.642.0 Chrome/10.0.642.0 Safari/534.16",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.639.0 Safari/534.16",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.638.0 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.634.0 Safari/534.16",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.634.0 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 SUSE/10.0.626.0 (KHTML, like Gecko) Chrome/10.0.626.0 Safari/534.16",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.15 (KHTML, like Gecko) Chrome/10.0.613.0 Safari/534.15",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.613.0 Chrome/10.0.613.0 Safari/534.15",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Gecko) Ubuntu/10.04 Chromium/10.0.612.3 Chrome/10.0.612.3 Safari/534.15",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Gecko) Chrome/10.0.612.1 Safari/534.15",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.611.0 Chrome/10.0.611.0 Safari/534.15",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/10.0.602.0 Safari/534.14",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/10.0.601.0 Safari/534.14",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/10.0.601.0 Safari/534.14",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML,like Gecko) Chrome/9.1.0.0 Safari/540.0",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML, like Gecko) Ubuntu/10.10 Chrome/9.1.0.0 Safari/540.0",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.601.0 Safari/534.14",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Ubuntu/10.10 Chromium/9.0.600.0 Chrome/9.0.600.0 Safari/534.14",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.600.0 Safari/534.14",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.599.0 Safari/534.13",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.84 Safari/534.13",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.44 Safari/534.13",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.19 Safari/534.13",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.15 Safari/534.13",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.15 Safari/534.13",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.596.0 Safari/534.13",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Ubuntu/10.04 Chromium/9.0.595.0 Chrome/9.0.595.0 Safari/534.13",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Ubuntu/9.10 Chromium/9.0.592.0 Chrome/9.0.592.0 Safari/534.13",
        "Mozilla/5.0 (X11; U; Windows NT 6; en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.587.0 Safari/534.12",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.579.0 Safari/534.12",
        "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.576.0 Safari/534.12",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML, like Gecko) Ubuntu/10.10 Chrome/8.1.0.0 Safari/540.0",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.558.0 Safari/534.10",
        "Mozilla/5.0 (X11; U; CrOS i686 0.9.130; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.344 Safari/534.10",
        "Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.343 Safari/534.10",
        "Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.341 Safari/534.10",
        "Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.339 Safari/534.10",
        "Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.339",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Ubuntu/10.10 Chromium/8.0.552.237 Chrome/8.0.552.237 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; de-DE) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.3 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/533.3",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.215 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.215 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.215 Safari/534.10",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.210 Safari/534.10",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.200 Safari/534.10",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.551.0 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.548.0 Safari/534.10",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.544.0 Safari/534.10",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.15) Gecko/20101027 Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.540.0 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.540.0 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; de-DE) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.540.0 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.540.0 Safari/534.10",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.9 (KHTML, like Gecko) Chrome/7.0.531.0 Safari/534.9",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.8 (KHTML, like Gecko) Chrome/7.0.521.0 Safari/534.8",
        "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.517.24 Safari/534.7",
        "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7",
        "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.6 (KHTML, like Gecko) Chrome/7.0.500.0 Safari/534.6",
    ];
}
