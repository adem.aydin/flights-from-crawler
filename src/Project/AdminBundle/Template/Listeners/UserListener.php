<?php
/**
 * Created by PhpStorm.
 * User: hursit
 * Date: 04/09/15
 * Time: 20:39
 */
namespace Project\AdminBundle\Template\Listeners;

use Avanzu\AdminThemeBundle\Event\ShowUserEvent;
use Avanzu\AdminThemeBundle\Model\UserModel;
use FOS\UserBundle\Model\User;
use Symfony\Component\DependencyInjection\Container;

class UserListener {
    /** @var Container $container */
    protected $container;

    function __construct(Container $container)
    {
        $this->container = $container;
    }


    public function onShowUser(ShowUserEvent $event) {
        /** @var User $userEntity */
        $userEntity = $this->container->get('security.context')->getToken()->getUser() ?: false;

        $userModel = new UserModel();
        $userModel
            ->setName($userEntity->getUsername())
            #->setAvatar($this->container->get('templating.helper.assets')->getUrl('bundles/enuygunadmin/img/online-user.png'))
            ->setIsOnline(true)
            ->setMemberSince($userEntity->getLastLogin());

        $event->setUser($userModel);
    }
}