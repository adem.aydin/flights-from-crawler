<?php
/**
 * Created by PhpStorm.
 * User: hursit
 * Date: 04/09/15
 * Time: 20:39
 */
namespace Project\AdminBundle\Template\Listeners;


use Project\AdminBundle\Template\Models\MenuItemModel;
use Doctrine\Common\Collections\ArrayCollection;
use Avanzu\AdminThemeBundle\Event\SidebarMenuEvent;
use FOS\UserBundle\Model\User;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;

class MenuListener {
    /** @var Container $container */
    protected $container;
    /** @var  ArrayCollection $menuTree */
    protected $menuTree;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->menuTree = new ArrayCollection();
    }


    public function onSetupMenu(SidebarMenuEvent $event)
    {
        $request = $event->getRequest();

        foreach ($this->getMenu($request) as $item) {
            $event->addItem($item);
        }
    }

    protected function getMenu(Request $request)
    {
        /** @var User $user */
        $user = $this->container->get('security.context')->getToken()->getUser();

        $logout = new MenuItemModel('logout', 'Çıkış', 'fos_user_security_logout', array(), 'fa fa-sign-out');

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            $dashboard = new MenuItemModel('admin_default_index', 'Anasayfa', 'admin_default_index', array(), 'fa fa-dashboard');

#            $application = new MenuItemModel('admin_application_index', 'Başvurular', 'admin_application_index', array(), 'fa fa-plane');

#            $application
#                ->addChild(new MenuItemModel('admin_application_index', 'Liste', 'admin_application_index', array()))


            $this->insertMenu(
                $dashboard,
                $logout
            );
        }

        return $this->activateByRoute($request->get('_route'), $this->menuTree);
    }

    public function insertMenu()
    {
        $rootMenus = func_get_args();

        /** @var MenuItemModel $menuItemModel */
        foreach ($rootMenus as $menuItemModel) {
            if (!$this->menuTree->get($menuItemModel->getIdentifier())) {
                $this->menuTree->set($menuItemModel->getIdentifier(), $menuItemModel);
            } else {
                /** @var MenuItemModel $menu */
                $menu = $this->menuTree->get($menuItemModel->getIdentifier());

                if ($menuItemModel->hasChildren()) {
                    /** @var MenuItemModel $child */
                    foreach ($menuItemModel->getChildren() as $child) {
                        if (!$menu->hasChildren($child->getIdentifier())) {
                            $menu->addChild($child);
                        }
                    }
                }
            }
        }
    }

    protected function activateByRoute($route, $items) {

        foreach($items as $item) { /** @var $item MenuItemModel */
            if($item->hasChildren()) {
                $this->activateByRoute($route, $item->getChildren());
            }
            else {
                if($item->getRoute() == $route) {
                    $item->setIsActive(true);
                }
            }
        }

        return $items;
    }


}