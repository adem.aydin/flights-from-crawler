<?php

namespace Project\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="avanzu_admin_home")
     * @Route("/", name="admin_default_index")
     */
    public function indexAction()
    {
        return $this->render('ProjectAdminBundle:Default:index.html.twig');
    }
}
