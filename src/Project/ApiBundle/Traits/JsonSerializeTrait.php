<?php
/**
 * Created by PhpStorm.
 * User: hursittopal
 * Date: 2019-01-10
 * Time: 01:05
 */

namespace Project\ApiBundle\Traits;


trait JsonSerializeTrait
{
    public static $FLOAT_PRECISION = 2;

    public static $_IGNORE_PREFIX = '_';

    public function jsonSerialize()
    {
        $variables = get_object_vars($this);

        $result = [];

        foreach ($variables as $key => $value) {
            if (substr($key, 0, 1) === self::$_IGNORE_PREFIX) {
                continue;
            }

            if ($value instanceof \Doctrine\Common\Collections\Collection) {
                $value = $value->toArray();
            }

            if ($value instanceof \DateTime) {
                $value = $value->format('Y-m-d H:i:s');
            }

            if (is_float($value)) {
                $value = round($value, self::$FLOAT_PRECISION, PHP_ROUND_HALF_UP);
            }

            $result[self::camelCaseToUnderscore($key)] = $value;
        }

        return $result;
    }

    static function camelCaseToUnderscore($input)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }
}