<?php
/**
 * Created by PhpStorm.
 * User: hursittopal
 * Date: 2019-01-10
 * Time: 01:04
 */

namespace Project\ApiBundle\Schema;


use Project\ApiBundle\Traits\JsonSerializeTrait;

class Result implements \JsonSerializable
{
    use JsonSerializeTrait;
}