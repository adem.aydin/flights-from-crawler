<?php

namespace Project\ApiBundle\Controller;

use Project\ApiBundle\Schema\Result;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $schema = new Result();

        $data = [
            'status' => 'success',
            'result' => $schema
        ];

        return new JsonResponse($data);
    }
}
