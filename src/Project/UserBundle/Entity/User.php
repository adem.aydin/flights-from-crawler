<?php
namespace Project\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=TRUE)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=TRUE)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="citizen_number", type="string", length=255, nullable=TRUE)
     */
    private $citizenNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=TRUE)
     */
    private $phoneNumber;


    /**
     * @var string
     *
     * @ORM\Column(name="birth_day", type="date")
     */
    private $birthDay;

    public function __construct()
    {
        parent::__construct();

        $this->applications = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getCitizenNumber()
    {
        return $this->citizenNumber;
    }

    /**
     * @param string $citizenNumber
     */
    public function setCitizenNumber($citizenNumber): self
    {
        $this->citizenNumber = $citizenNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * @param string $birthDay
     */
    public function setBirthDay($birthDay): self
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return User
     */
    public function setLastname(string $lastname): User
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPasswordSet()
    {
        return '';
    }

    public function setPasswordSet($passwordSet)
    {
        return '';
    }
}
