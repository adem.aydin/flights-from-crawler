<?php

namespace Deployer;

use Symfony\Component\Console\Input\InputArgument;

require 'recipe/symfony.php';

set('application', 'freelance');

set('repository', 'https://gitlab.com/adem.aydin/flights-from-crawler.git');

set('git_tty', true);

argument('site', InputArgument::OPTIONAL, 'Site');

set('shared_dirs', array_merge(get('shared_dirs'), ['web/sitemaps', 'app/cache', 'app/logs', 'web/uploads']));

#set('writable_dirs', array_merge(get('writable_dirs'), ['web/sitemaps', 'app/cache', 'app/logs', 'web/uploads']));

set('keep_releases', 2);

host(
    '127.0.0.1',
    '167.99.105.60',
    '159.89.129.210',
    '178.128.76.180',
    '178.128.74.203',
    '142.93.31.57'
)
    ->user('root')
    ->set('http_user', 'www-data')
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/html/crawler')
;


task('pwd', function () {
    $result = run('pwd');
    writeln("Current dir: $result");
});

task('build', function () {
    run('cd {{release_path}} && build');
});

after('deploy:failed', 'deploy:unlock');

